package com.virtualassist.fragments;

import android.os.Bundle;

import com.virtualassist.R;

/**
 * Created by Hemant on 04-12-2016.
 */

public class SettingsFragment extends AppPreferenceFragment {
    public static final String FRAGMENT_TAG = "preference_fragment";

    public SettingsFragment() {
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings, rootKey);
    }
}
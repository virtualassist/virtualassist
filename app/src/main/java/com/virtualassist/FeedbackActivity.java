package com.virtualassist;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.jaredrummler.materialspinner.MaterialSpinner;

public class FeedbackActivity extends AppCompatActivity {
    private MaterialSpinner materialSpinner;
    private EditText mName, mEmail, mMessage;
    private static final String[] FEEDLIST ={"General Feedback", "Bug Report", "Feature Request"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mName = (EditText) findViewById(R.id.feed_txt_name);
        mEmail = (EditText) findViewById(R.id.feed_txt_mail);
        mMessage = (EditText) findViewById(R.id.feed_txt_msg);

        materialSpinner = (MaterialSpinner) findViewById(R.id.spinner);
        materialSpinner.setItems(FEEDLIST);
        materialSpinner.setGravity(Gravity.CENTER_VERTICAL);
        materialSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send, menu);
        MenuItem sendItem = menu.findItem(R.id.action_send);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                finish();
                return true;
            case R.id.action_send:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
